﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeBase : MonoBehaviour {
	float velocity;
	float minVelocity = 0.25f;
	float maxVelocity = 1.25f;
	Animator animator;
	public GameObject spike;
	GameObject currentSpike;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator>();
		animator.SetFloat("velocity", 0.0f);
	}

	void StartSlide(){
		StartCoroutine("Slide");
	}

	void StopGame(){
		StopAllCoroutines();
		animator.SetFloat("velocity", 0.0f);

		if(currentSpike)
			Destroy(currentSpike);
	}

	// Slide up and down at a random rate
	IEnumerator Slide(){
		yield return new WaitForSeconds(60.0f); // Wait 10 seconds before sliding
		StartCoroutine("SpawnSpike");

		while(true){
			yield return new WaitForSeconds(1f);
			velocity = Random.Range(minVelocity, maxVelocity);
			animator.SetFloat("velocity", velocity);
		}
	}

	IEnumerator SpawnSpike(){
		Vector3 currentPosition;

		while(true){
			yield return new WaitForSeconds(Random.Range(7f, 15f));
			currentPosition = transform.position;
			currentPosition.z = -1f;
			currentSpike = Instantiate(spike, currentPosition, transform.rotation);
		}
	}
}
