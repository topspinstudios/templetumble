using UnityEngine;

public class RockCrackParticles : MonoBehaviour {
	public ParticleSystem smoke;
	public ParticleSystem rockShards;

	void EmitParticles (Vector3 pos) {
		transform.position = pos;
		smoke.Emit(5);
		rockShards.Emit(5);
	}
}
