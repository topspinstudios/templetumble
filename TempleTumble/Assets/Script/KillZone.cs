﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillZone : MonoBehaviour {

	GameObject gameController;
	string objTag;

	void Start(){
		gameController = transform.root.root.gameObject;
	}

	void OnCollisionEnter2D(Collision2D col){
		objTag = col.gameObject.tag;
		//print("Killzone collided with " + objTag);

		if(objTag == "Beam"){
			//Tell game controller the game is over
			gameController.SendMessage("GameOver");
		}

		else if(objTag == "Rock")
			col.gameObject.SendMessage("SelfDestruct", false); // Tell the rock to kill itself

		else if(objTag == "RockCrusher")
			col.gameObject.SendMessage("SelfDestruct");
	}
}
