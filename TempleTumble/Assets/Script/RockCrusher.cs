using System.Collections;
using UnityEngine;

public class RockCrusher : FallingObject {

	PointEffector2D pointEffector2D;
	// Use this for initialization
	void Start () {
		pointEffector2D = GetComponent<PointEffector2D>();
		//StartCoroutine("Explode");
		Destroy(this.gameObject, 4f);
	}

	void OnCollisionEnter2D(Collision2D col){
		if(col.gameObject.tag == "Rock")
			col.gameObject.SendMessage("SelfDestruct"); // self destruct and create a broken rock
	}

	IEnumerator Explode(){
		yield return new WaitForSeconds(3.5f);
		pointEffector2D.enabled = true;
		yield return new WaitForSeconds(0.5f);
		Destroy(this.gameObject);
	}
}
