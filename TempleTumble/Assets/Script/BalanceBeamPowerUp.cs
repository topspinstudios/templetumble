﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalanceBeamPowerUp : PowerUp {
	public GameObject beamPrefab;

	void Start(){
		coolDownTime = 12f;
	}

	public void ActivateBalance(){
		if(isCooled){
			beamPrefab.SendMessage("Balance");
			StartCoolDown();
		}
	}
}
