﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Achievements : MonoBehaviour {
	public Text highScoreText;
	private int highScore, coinsCollected, rageCount;

	public Image[] banners;

	void UpdateAchievements () {
		rageCount = PlayerPrefs.GetInt("RageCount");
		highScore = PlayerPrefs.GetInt("HighScore");
		coinsCollected = PlayerPrefs.GetInt("CoinsCollected");

		highScoreText.text = "High Score: " + highScore;

		banners[0].color = rageCount > 50 ? Color.white : Color.gray;
		banners[1].color = highScore > 20000 ? Color.white : Color.gray;
		banners[2].color = coinsCollected > 100 ? Color.white : Color.gray;
	}
}
