﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class RagePower : MonoBehaviour {
	public DataController dataController;
	public GameController gameController;
	public Image disabledRageButton;

	private int rageMaxTapCount;
	private int rageCurrentTapCount;
	private float maxRageTime;

	void Start(){
		rageMaxTapCount = dataController.rageMaxTapCount;
		maxRageTime = dataController.maxRageTime;
	}

	public void AddRage(){
		if((dataController.isRaging == false) && (rageCurrentTapCount < rageMaxTapCount)){
			rageCurrentTapCount++;
			disabledRageButton.fillAmount = 1f - ((float)rageCurrentTapCount / (float)rageMaxTapCount);
		}
	}

	public void ResetRage(){
		StopAllCoroutines();

		rageCurrentTapCount = 0;
		disabledRageButton.fillAmount = 1f;
		dataController.isRaging = false;
		gameController.TargetAllActiveRocks();
	}

	// Start rage if it's charged
	public void RageButton(){
		if(rageCurrentTapCount >= rageMaxTapCount && dataController.isRaging == false){
			print("Raging");
			dataController.isRaging = true;
			rageCurrentTapCount = 0;
			StartCoroutine("StartRageRoutine");
		}
	}

	// start raging and destroy rock on single tap
	IEnumerator StartRageRoutine(){
		float currentTime = 0f;
		float fillAmount = 1f;
		gameController.TargetAllActiveRocks();

		while(currentTime <= maxRageTime){
			yield return dataController.UpdateTime;

			fillAmount = (currentTime / maxRageTime);
			disabledRageButton.fillAmount = fillAmount;

			currentTime += dataController.UpdateTime;
		}

		ResetRage();
	}
}
