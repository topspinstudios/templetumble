using System.Collections;
using System.Collections.Generic;

public class GameInfo{
  public float leftPlayableBound {get; set;}
  public float rightPlayableBound {get; set;}
  public float initialPositionY {get; set;}
}
