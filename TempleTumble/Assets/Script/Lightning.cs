﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lightning : MonoBehaviour {
	public Animator animator;
	public GameObject lightningPrefab;
	public BoxCollider2D boxCollider2D;
	public AudioSource audioSource;

	// Use this for initialization
	void Start () {
	}

	void Activate(Vector3 lightningPosition){
		audioSource.Play();
		lightningPrefab.SetActive(true);
		lightningPosition = new Vector3(lightningPosition.x, transform.position.y, transform.position.z);
		//print("lightning position: " + lightningPosition);
		StartCoroutine("LightningRoutine", lightningPosition);
	}

	IEnumerator LightningRoutine(Vector3 lightningPosition){
		animator.SetTrigger("LightningStrikeTrigger");
		transform.position = lightningPosition;
		yield return new WaitForSeconds(0.5f);
		boxCollider2D.enabled = true;

		yield return new WaitForSeconds(0.5f);
		boxCollider2D.enabled = false;

		lightningPrefab.SetActive(false);
	}

	// Update is called once per frame
	void OnTriggerEnter2D (Collider2D col) {
		if(col.gameObject.tag == "Rock")
			col.gameObject.SendMessage("SelfDestruct", false); // Tell the rock to kill itself

		else if(col.gameObject.tag == "RockCrusher")
			col.gameObject.SendMessage("SelfDestruct");
	}
}
