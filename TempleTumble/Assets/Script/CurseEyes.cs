using UnityEngine;

public class CurseEyes : MonoBehaviour {

	Animator animator;
	AudioSource audioSource;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator>();
		audioSource = GetComponent<AudioSource>();
	}

	void TriggerCurse(){
		animator.SetTrigger("Curse");
		audioSource.Play();
	}
}
