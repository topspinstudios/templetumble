using UnityEngine;
using System.Collections;

public class Spike : MonoBehaviour {
	Rigidbody2D body2D;
	float velocity;

	void Start(){
		Physics2D.IgnoreLayerCollision(9, 10);
		Physics2D.IgnoreLayerCollision(9, 11);
		velocity = Random.Range (20f, 50f);

		body2D = GetComponent<Rigidbody2D>();
		Destroy(this.gameObject, 2f);
		StartCoroutine("Move", (transform.rotation.z == 1 ? -1f : 1f));
	}

	IEnumerator Move(float rotation){
		Vector2 spikeVelocity = Vector2.right * velocity * rotation;
		while(true){
			body2D.velocity = spikeVelocity;

			yield return null;
		}
	}

	void OnCollisionEnter2D (Collision2D col) {
		if(col.gameObject.tag == "Rock"){
			col.gameObject.SendMessage("SelfDestruct"); // tell rock to self destruct
			Destroy(this.gameObject);
		}
		else if(col.gameObject.tag == "Spike"){
			Destroy(this.gameObject);
		}
	}
}
