﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningPowerUp : PowerUp {
	float maxAimTime = 0.5f;
	Vector3 summonLightningPosition;

	public GameObject lightningBoltPrefab;
	public GameObject crossHair;

	void Start(){
		coolDownTime = 10f;
	}

	public void ActivateLightning(){
		if(isCooled){
			StartCoroutine("Aim"); // cool down after the lightning strikes
		}
	}

	IEnumerator Aim(){
		Time.timeScale = 0.2f;
		Time.fixedDeltaTime = Time.timeScale * 0.02f;
		crossHair.SetActive(true);
		crossHair.transform.position = Vector3.zero;

		float currentAimingTime = 0.0f;
		bool hasSummonLightning = false;
		bool hasTouchedScreen = false;

		while(currentAimingTime < maxAimTime && !hasSummonLightning){
			if(Input.GetMouseButton(0)){
				summonLightningPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				crossHair.transform.position = new Vector3(summonLightningPosition.x, summonLightningPosition.y, 0f);
				hasTouchedScreen = true;
			}
			else if(Input.GetMouseButtonUp(0) && hasTouchedScreen){
				lightningBoltPrefab.SendMessage("Activate", summonLightningPosition);
				currentAimingTime = maxAimTime + 1;
				hasSummonLightning = true;
				continue;
			}
			currentAimingTime += Time.deltaTime;
			yield return null;
		}

		if(!hasSummonLightning)
			lightningBoltPrefab.SendMessage("Activate", Vector3.zero);

		EndLightning();
	}

	void EndLightning(){
		crossHair.SetActive(false);
		Time.timeScale = 1.0f;
		StartCoolDown();
	}
}
