using System.IO;
using UnityEngine;

public class FallingObject : MonoBehaviour{
  protected Rigidbody2D body2D;
  public AudioClip rockHitsRock, rockHitsBeam;
  AudioSource audioSource;

  protected float collisionMaxMagnitude;
  protected bool hasLanded;
  protected int identifier;

  void Awake(){
    body2D = GetComponent<Rigidbody2D>();
    audioSource = GetComponent<AudioSource>();
    hasLanded = false;
    collisionMaxMagnitude = 5f;
    transform.Rotate(0f, 0f, Random.Range(0f, 360f));
  }

  // Get Sound Effects from GameController
  protected void SetSFX(AudioClip[] SFX){
    this.rockHitsRock = SFX[0];
    this.rockHitsBeam = SFX[1];
  }

  protected void SetIdentifier(int id){
    identifier = id;
  }

  protected void PlayRockHitsBeamSFX(float volume){
    audioSource.clip = rockHitsBeam;
    audioSource.volume = volume;
    audioSource.Play();
  }

  protected void PlayRockHitsRockSFX(float volume){
    audioSource.clip = rockHitsRock;
    audioSource.volume = volume;
    audioSource.Play();
  }

  // Override if it needs to do more than self destruct
  protected void SelfDestruct(){
    Destroy(this.gameObject);
  }
}
