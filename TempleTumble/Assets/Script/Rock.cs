using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class Rock : FallingObject{
  bool hasWaitedForReplacement = false;
  bool isMoveable = true;
  public GameObject gameController;

  // particles to show the rock is being cracked
  public ParticleSystem rockShardPS;
  public ParticleSystem smokePS;

  float maxSize = 2f;
  float minSize = 1f;

  public GameObject outline; // shows that the rock can be moved
  public GameObject curseOutline; // shows that the rock can be moved

  float maxForceApply = 8000f;
  float maxVelocityX = 20f;

  Vector2 originalTouchPos, currentTouchPos;
  bool hasSpawnReplacement;
  bool isCursed;
  public GameObject smoke;
  bool isTouchingColumn;

  public GameObject target;
  public SpriteRenderer crackRenderer;
  Color crackColor;
  int health;
  int currentHealth;

  void Start(){
    //body2D.useAutoMass = false;
    crackColor = crackRenderer.color;
  }

  // Call this function to enable and disable the rock.
  void EnableRock(bool isEnabled = false){
    hasLanded = isEnabled;
    hasSpawnReplacement = isEnabled;
    isCursed = isEnabled;
    isTouchingColumn = isEnabled;
    outline.SetActive(isEnabled);
    isMoveable = isEnabled;
    EnableRigidbody(isEnabled);

    if(isEnabled){
      transform.localScale = Vector3.one * (Random.Range(minSize, maxSize));
      StartHealth();
    }
    else{
      StopAllCoroutines();
      curseOutline.SetActive(false);
    }
  }

  void AddSpeed(float maxForce){
    Vector3 randomForce = new Vector3(0f, Random.Range(-1f * maxForce, 0f), 0f);
    body2D.AddForce(randomForce, ForceMode2D.Impulse);
  }

  // Trigger the curse and reverse movement of rock
  void TriggerCurse(){
    isCursed = true;
    if(isMoveable){
      outline.SetActive(false);
      curseOutline.SetActive(true);
    }
  }

  void ChangeToRandomSize(){
    float randomNum = Random.Range(0f, 100f);

    StartCoroutine("ChangeSizeRoutine", randomNum < 50 ? Random.Range(0.25f, 0.5f) : Random.Range(1.5f, 3f));
  }

  IEnumerator ChangeSizeRoutine(float toScale){
    Vector3 newScale = Vector3.one * toScale;
    Vector3 startScale = transform.localScale;
    float currentTime = 0f;
    float changeSizeTime = 0.4f;

    while(transform.localScale != newScale || currentTime >= changeSizeTime){
      transform.localScale = Vector3.Lerp(startScale, newScale, (currentTime / changeSizeTime));
      yield return null;
      currentTime += Time.fixedDeltaTime;
    }
  }

  void StartHealth(){
    float scaleFactor = transform.localScale.x;
    float healthFactor = Mathf.Lerp(7, 20, ((scaleFactor - minSize) / (maxSize - minSize)));
    health = (int)(healthFactor);
    //print("scale: " + scaleFactor + "\thealth: " + health);
    currentHealth = health;
  }

  void SetGameController(GameObject gameController){
    this.gameController = gameController;
  }

  // The initial start of the rock.
  void StartMovement(){
    EnableRock(true);
    StartCoroutine("MoveRock");
  }

  IEnumerator MoveRock(){
    float velocityRatio = 0; // ratio is calculated by dividing currentRockVelocityX by maxVelocityX and clamp it
    float currentRockVelocityX = 0; // takes the current velocity of rock using body2D
    float currentPlayerInput = 0; // value between 0, 1
    float currentForce = 0;
    Vector2 force = new Vector2(0f, 0f);

    //Time.fixedDeltaTime = Time.timeScale * 0.03f;

    while(isMoveable){
      currentPlayerInput = CrossPlatformInputManager.GetAxis("Horizontal");
      currentRockVelocityX = body2D.velocity.x;

      // Only add force when it's not touching a column
      if(isTouchingColumn){
        if(currentPlayerInput > 0 && transform.position.x > 14){
          yield return null;
          continue;
        }

        else if(currentPlayerInput < 0 && transform.position.x < -14){
          yield return null;
          continue;
        }
      }

      // Only add force if player input is detected
      if(currentPlayerInput != 0 && isMoveable){
          velocityRatio = 1 - Mathf.Clamp(Mathf.Abs(currentRockVelocityX / maxVelocityX), 0f, 1f); // clamp param -> value, min, max
          currentForce = (maxForceApply * currentPlayerInput * body2D.mass) * velocityRatio;
          force.x = isCursed ? -currentForce : currentForce;
          body2D.AddForce(force);
      }

      yield return null;
    }
  }

  void OnCollisionEnter2D(Collision2D col){
    string objectCollidedTag = col.gameObject.tag;
    if(objectCollidedTag != "Column"){
      if((objectCollidedTag == "Beam" || objectCollidedTag == "Rock") && !hasLanded){
        float volume = col.relativeVelocity.magnitude / collisionMaxMagnitude;
        if(objectCollidedTag == "Beam"){
          PlayRockHitsBeamSFX(volume > 1.0f ? 1.0f : volume);
        }
        else
          PlayRockHitsRockSFX(volume > 1.0f ? 1.0f : volume);

        if(Mathf.Abs(body2D.velocity.y) < 4f)
          hasLanded = true;

        isMoveable = false;
        outline.SetActive(false);
        //print ("deactivating outline line 145");
      }
      if(isMoveable){
        isMoveable = false;
        StopCoroutine("MoveRock");
        //gameController.SendMessage("EnableTouchDetection");
      }
      if(!hasSpawnReplacement && hasWaitedForReplacement){
        gameController.SendMessage("SummonRock");
        hasSpawnReplacement = true;
      }
      outline.SetActive(false);
      curseOutline.SetActive(false);
      //print ("deactivating outline line 158");
    }
    else {
      isTouchingColumn = true;
    }
  }

  void OnCollisionExit2D(Collision2D col){
    if(col.gameObject.tag == "Column")
      isTouchingColumn = false;
  }

  void EnableRigidbody(bool isEnabled = false){
    body2D.bodyType = isEnabled ? RigidbodyType2D.Dynamic : RigidbodyType2D.Static;
    body2D.simulated = isEnabled;
  }



  // Override parent method
  new void SelfDestruct(){
    Instantiate(smoke, this.transform.position, this.transform.rotation);

    EnableRock(false);
  }

  void CrackRock(){
    if(hasLanded){
      currentHealth--;

      if(currentHealth <= 0){
        SelfDestruct();
      }

      crackColor.a = 1f - ((float)currentHealth / (float)health);
      crackRenderer.color = crackColor;

      if(Random.Range(0f, 100f) < 30f){
        EmitRockShards();
      }
    }
  }

  void CrushRock(){
    SelfDestruct();
  }

  void EnableRage(bool isEnabled){
    target.SetActive(isEnabled);

    if(isEnabled){
      StartCoroutine("ShrinkTarget");
    }
    else{
      StopCoroutine("ShrinkTarget");
    }
  }

  IEnumerator ShrinkTarget(){
    target.transform.localScale = Vector3.one;

    float currentTime = 0f;
    float shrinkTime = 0.5f;
    Vector3 targetSize = Vector3.one * 0.5f;

    while(target.transform.localScale.x > targetSize.x){
      yield return null;
      target.transform.localScale = Vector3.Lerp(Vector3.one, targetSize, currentTime / shrinkTime);
      currentTime += Time.fixedDeltaTime;
      if(currentTime >= shrinkTime)
        break;
    }
  }

  // Emit some rock shards when the rock is cracked
  void EmitRockShards(){
    rockShardPS.Emit(5);
    smokePS.Emit(5);
  }
}
