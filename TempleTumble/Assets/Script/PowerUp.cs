﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUp : MonoBehaviour {
	[SerializeField]
	float imageFillAmount;

	protected bool isCooled;
	protected float coolDownTime;
	public Image powerUpImage;
	float updateTimeInterval = 0.1f;
	public GameObject uiController;

	[SerializeField]
	protected float currentCoolDownTime;

	// Starting game requires power up to cool and stop any previous coroutine
	void StartGame () {
		isCooled = true;
		powerUpImage.fillAmount = 1;
		currentCoolDownTime = coolDownTime;
	}

	void StopGame(){
		StopAllCoroutines();
	}

	protected void StartCoolDown(){
		isCooled = false;
		StartCoroutine("CoolDownRoutine");

		uiController.SendMessage("UsePowerUp");
	}

	protected IEnumerator CoolDownRoutine(){
		imageFillAmount = 0.0f;
		powerUpImage.fillAmount = imageFillAmount;

		while(!isCooled){
			yield return new WaitForSeconds(updateTimeInterval);
			imageFillAmount += (updateTimeInterval / currentCoolDownTime);
			powerUpImage.fillAmount = imageFillAmount;
			isCooled = imageFillAmount >= 1 ? true : false;
		}

		currentCoolDownTime += 1.0f; // Increase cool down time after every use
	}
}
