﻿using System.Collections;
using UnityEngine;

public class RockMoveTut : RockTut {
	public GameObject movementController;
	float lowestYPos = -4f;

	void OnEnable(){
		spriteRen.color = opaque;
	}

	new void Reset(){
		base.Reset();
		StartCoroutine("FallRoutine");
	}

	void OnCollisionEnter2D(Collision2D col){
		//print("collided with " + col.gameObject.tag);
		if(col.gameObject.tag == "MoveTutorialBound"){
			movementController.SendMessage("NextMovement");
			transform.position = origin;
		}
	}

	IEnumerator FallRoutine(){
		Vector3 fallSpeed = new Vector3(0f, 0.075f, 0f);

		while(true){
			transform.position -= fallSpeed;
			if(transform.position.y < lowestYPos)
				transform.position = origin;

			yield return null;
		}
	}
}
