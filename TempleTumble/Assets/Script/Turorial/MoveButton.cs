﻿using System.Collections;
using UnityEngine;

public class MoveButton : ControlButtonTut {
	public GameObject rock;
	Vector3 moveSpeed;
	public bool isLeftButton;

	// Use this for initialization
	void Start () {
		moveSpeed = new Vector3(0.15f * (isLeftButton ? -1 : 1), 0f, 0f);
	}

	void EnableButton(bool isEnabled){
		//print(gameObject.name + " enabling button: " + isEnabled);
		this.isEnabled = isEnabled;
	}

	void OnMouseDown () {
		if(isEnabled){
			//print("moving button clicked");
			StartCoroutine("MoveRock");
			controller.SendMessage("ButtonClicked");
		}
	}

	void OnMouseUp(){
		StopAllCoroutines();
	}

	IEnumerator MoveRock(){
		while(true){
			rock.transform.position += moveSpeed;
			yield return null;
		}
	}
}
