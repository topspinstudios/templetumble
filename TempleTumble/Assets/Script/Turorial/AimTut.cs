﻿using System.Collections;
using UnityEngine;

public class AimTut : MonoBehaviour {
	public GameObject lightningController;

	void OnMouseDown () {
		lightningController.SendMessage("Aimed");
	}
}
