﻿using System.Collections;
using UnityEngine;

public class BeamTut : MonoBehaviour {
	void RotateBeam(){
		StartCoroutine("Rotate");
	}

	void Reset(){
		StopAllCoroutines();
		transform.rotation = Quaternion.Euler(0f, 0f, 0f);
	}

	IEnumerator Rotate(){
		float currentTime = 0;
		float totalBalanceTime = 2.0f;

		Quaternion startRotation = transform.rotation;
		Quaternion zero = Quaternion.Euler(0f, 0f, 0f);

		while(currentTime <= 2.0f){
			transform.rotation = Quaternion.Slerp(startRotation, zero,  (currentTime / totalBalanceTime));
			currentTime += Time.fixedDeltaTime;
			yield return null;
		}
	}

	void SetRandomRotation(){
		float rotationMultiplier = Random.value;
		Quaternion randomRotation = Quaternion.Euler(0, 0f, Random.Range(20f, 40f) * (rotationMultiplier > 0.5f ? 1f : -1f));

		transform.rotation = randomRotation;
	}
}
