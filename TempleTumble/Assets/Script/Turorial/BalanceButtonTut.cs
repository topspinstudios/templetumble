﻿using System.Collections;
using UnityEngine;

public class BalanceButtonTut : ControlButtonTut {

	void OnMouseDown () {
		if(isEnabled){
			controller.SendMessage("ButtonClicked");
			this.Click();
			isEnabled = false;
		}
	}
}
