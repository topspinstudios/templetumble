﻿using System.Collections;
using UnityEngine;

public class LightningCtrlTut : MonoBehaviour {
	public GameObject rocksHolder;
	public GameObject rock1;
	public GameObject rock2;
	public GameObject lightningButton;
	public GameObject crossHair;
	public GameObject hand;
	public GameObject lightning;

	public Transform lightningButtonPos;
	public Transform crossHairOrigin;
	public Transform aimPos;
	public Transform crossHairAimPos;
	private bool hasPressedButton;

	// Use this for initialization
	void StartAnimation () {
		StopAllCoroutines();
		hasPressedButton = false;
		crossHair.SetActive(false);
		lightning.SendMessage("Reset");

		rock1.SendMessage("Reset");
		rock2.SendMessage("Reset");
		hand.SendMessage("Reset");
		lightningButton.SendMessage("EnableButton");
		lightningButton.SendMessage("ReleaseClick");
		StartCoroutine("AnimateHand");
	}

	void AnimateHand(){
		hand.SendMessage("AnimateHand", !hasPressedButton ? lightningButtonPos.position : aimPos.position);
	}

	void ButtonClicked(){
		hasPressedButton = true;
		hand.SendMessage("HideHand");
		crossHair.SetActive(true);
		crossHair.transform.position = crossHairOrigin.position;
		AnimateHand();
		rocksHolder.SendMessage("EnableAim");
		lightningButton.SendMessage("DisableButton");
	}

	void Aimed(){
		crossHair.transform.position = crossHairAimPos.position;
		hasPressedButton = false;
		hand.SendMessage("HideHand");

		StartCoroutine("DelayLightning");
	}

	IEnumerator DelayLightning(){
		lightningButton.SendMessage("Click");
		StartCoroutine("CoolDown");
		yield return new WaitForSeconds(0.25f);
		crossHair.SetActive(false);
		lightning.SendMessage("PlayAnim");

		yield return new WaitForSeconds(0.25f);
		rock1.SendMessage("LightningDestroy");
		rock2.SendMessage("LightningDestroy");
	}

	IEnumerator CoolDown(){
		yield return new WaitForSeconds(2.0f);
		lightningButton.SendMessage("ReleaseClick");
		lightningButton.SendMessage("EnableButton");
		rock1.SendMessage("LightningResetAlpha");
		rock2.SendMessage("LightningResetAlpha");


		yield return new WaitForSeconds(1.0f);
		AnimateHand();
	}
}
