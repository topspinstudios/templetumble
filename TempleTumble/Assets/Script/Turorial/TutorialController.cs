﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TutorialController : MonoBehaviour {
	public GameObject[] tutorialControllers;
	public GameObject background;
	public Image[] pageIndicator;

	int currentControllerIndex;

	public void StartTutorial () {
		currentControllerIndex = 0;
		background.SetActive(true);

		tutorialControllers[currentControllerIndex].SetActive(true);
		tutorialControllers[currentControllerIndex].SendMessage("StartAnimation");

		pageIndicator[currentControllerIndex].color = Color.green;
	}

	public void StopTutorial(){
		background.SetActive(false);
		pageIndicator[currentControllerIndex].color = Color.gray;

		for(int i = 0; i < tutorialControllers.Length; i++){
			tutorialControllers[currentControllerIndex].SetActive(false);
		}
	}

	public void NextController(){
		pageIndicator[currentControllerIndex].color = Color.gray;
		currentControllerIndex = (currentControllerIndex + 1) % tutorialControllers.Length;

		pageIndicator[currentControllerIndex].color = Color.green;

		for(int i = 0; i < tutorialControllers.Length; i++){
			if(i == currentControllerIndex){
				tutorialControllers[i].SetActive(true);
				tutorialControllers[i].SendMessage("StartAnimation");
			}
			else{
				tutorialControllers[i].SetActive(false);
			}
		}
	}

	public void PreviousController(){
		pageIndicator[currentControllerIndex].color = Color.gray;

		currentControllerIndex = currentControllerIndex - 1;
		currentControllerIndex = currentControllerIndex < 0 ? (tutorialControllers.Length - 1) : currentControllerIndex;

		pageIndicator[currentControllerIndex].color = Color.green;

		print("current index: " + currentControllerIndex);

		for(int i = 0; i < tutorialControllers.Length; i++){
			if(i == currentControllerIndex){
				tutorialControllers[i].SetActive(true);
				tutorialControllers[i].SendMessage("StartAnimation");
			}
			else{
				tutorialControllers[i].SetActive(false);
			}
		}
	}
}
