﻿using System.Collections;
using UnityEngine;

public class BalanceCtrlTut : MonoBehaviour {
	public GameObject balanceButton;
	public GameObject hand;
	public GameObject beam;

	public Transform buttonHandPos;

	void StartAnimation(){
		StopAllCoroutines();
		beam.SendMessage("Reset");
		hand.SendMessage("Reset");
		balanceButton.SendMessage("Reset");

		balanceButton.SendMessage("EnableButton");
		beam.SendMessage("SetRandomRotation");
		hand.SendMessage("AnimateHand", buttonHandPos.position);
	}

	void ButtonClicked(){
		hand.SendMessage("HideHand");
		beam.SendMessage("Rotate");

		StartCoroutine("DelayRestart");
	}

	IEnumerator DelayRestart(){
		yield return new WaitForSeconds(2.5f);
		balanceButton.SendMessage("ReleaseClick");

		yield return new WaitForSeconds(1.0f);
		StartAnimation();
	}
}
