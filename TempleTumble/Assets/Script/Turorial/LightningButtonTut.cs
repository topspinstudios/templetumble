﻿using System.Collections;
using UnityEngine;

public class LightningButtonTut : ControlButtonTut {

	void OnMouseDown () {
		if(isEnabled){
			controller.SendMessage("ButtonClicked");
			isEnabled = false;
		}
	}
}
