﻿using System.Collections;
using UnityEngine;

public class LightningAnim : MonoBehaviour {
	Animator animator;

	void PlayAnim () {
		Reset();
		
		if(!animator)
			animator = gameObject.GetComponent<Animator>();

		animator.SetTrigger("LightningStrikeTrigger");
	}

	void Reset(){
		if(!animator)
			animator = gameObject.GetComponent<Animator>();

		animator.StopPlayback();
	}
}
