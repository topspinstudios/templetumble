﻿using System.Collections;
using UnityEngine;

public class RageButtonTut : ControlButtonTut {
	int currentRage;
	int maxRage = 8;
	bool isRaging;
	Color gray = new Color(0.25f, 0.25f, 0.25f, 1f);

	new void Reset(){
		base.Reset();
		currentRage = 0;
		isRaging = false;
		spriteRen.color = gray;
	}

	void OnMouseDown () {
		if(isEnabled){
			controller.SendMessage("StartRage");
			isEnabled = false;
			StartCoroutine("Rage");
			controller.SendMessage("StopHandAnim");
		}
	}

	IEnumerator Rage(){
		isRaging = true;
		yield return new WaitForSeconds(3f);
		controller.SendMessage("EndRage");
		currentRage = 0;
		isRaging = false;
		spriteRen.color = gray;
	}

	void AddRage(){
		if(isRaging == false){
			currentRage += currentRage < maxRage ? 1 : 0;
			if(currentRage >= maxRage){
				isEnabled = true;
				spriteRen.color = Color.white;
				controller.SendMessage("PointToRageButton");
			}
		}
	}
}
