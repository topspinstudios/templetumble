﻿using System.Collections;
using UnityEngine;

public class RockTut : MonoBehaviour {
	protected Vector3 origin;
	public GameObject smoke;
	protected SpriteRenderer spriteRen;
	public SpriteRenderer outline;

	protected Color opaque;
	protected Color transparent;

	Vector3 moveSpeed;

	void Awake(){
		SetRenderer();
		origin = transform.position;
		opaque = new Color(1f, 1f, 1f, 1f);
		transparent = new Color(1f, 1f, 1f, 0f);
	}

	protected void Reset(){
		StopAllCoroutines();
		transform.position = origin;
		if(!spriteRen)
			SetRenderer();
		spriteRen.color = opaque;
	}

	void SetRenderer(){
		spriteRen = gameObject.GetComponent<SpriteRenderer>();
	}

	void LightningDestroy(){
		if(!spriteRen)
			SetRenderer();
		spriteRen.color = transparent;

		Instantiate(smoke, this.transform.position, this.transform.rotation);
	}

	protected void LightningResetAlpha(){
		if(!spriteRen)
			SetRenderer();
		spriteRen.color = opaque;
	}
}
