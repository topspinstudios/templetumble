﻿using System.Collections;
using UnityEngine;

public class ControlButtonTut : MonoBehaviour {
	public GameObject controller;
	protected SpriteRenderer spriteRen;
	protected bool isEnabled;

	void SetRenderer(){
		spriteRen = gameObject.GetComponent<SpriteRenderer>();
	}

	protected void Click(){
		if(!spriteRen)
			SetRenderer();

		spriteRen.color = Color.gray;
	}

	protected void ReleaseClick(){
		if(!spriteRen)
			SetRenderer();

		spriteRen.color = Color.white;
	}

	protected void Reset(){
		ReleaseClick();
		isEnabled = false;
	}

	void EnableButton(){
		isEnabled = true;
	}

	void DisableButton(){
		isEnabled = false;
	}
}
