﻿using System.Collections;
using UnityEngine;

public class RageCtrlTut : MonoBehaviour {
	public GameObject[] rockArray;
	public GameObject rageButton;
	public GameObject hand;
	public Transform rageHandTransform;
	public Transform rockHandTransform;

	void StartAnimation () {
		Reset();
		hand.SendMessage("Reset");
		PointHandToRock();
	}

	void PointHandToRock(){
		hand.SendMessage("AnimateHand", rockHandTransform.position);
	}

	void StopHandAnim(){
		hand.SendMessage("HideHand");
	}

	void Reset(){
		StopAllCoroutines();
		foreach(GameObject rock in rockArray){
			rock.SendMessage("Reset");
		}
		rageButton.SendMessage("Reset");
	}

	void StartRage(){
		foreach(GameObject rock in rockArray){
			rock.SendMessage("StartRage");
		}
	}

	void PointToRageButton(){
		hand.SendMessage("AnimateHand", rageHandTransform.position);
	}

	void EndRage(){
		foreach(GameObject rock in rockArray){
			rock.SendMessage("EndRage");
		}
	}
}
