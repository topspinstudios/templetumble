﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementCtrlTut : MonoBehaviour {
	public GameObject rock;
	public GameObject leftButton;
	public GameObject rightButton;
	public GameObject hand;

	public Transform leftButtonHandPos;
	public Transform rightButtonHandPos;

	private bool isMovingLeft;

	// Use this for initialization
	void StartAnimation () {
		Reset();
		isMovingLeft = true;
		StartCoroutine("GuideHandLeftButton");
	}

	void Reset(){
		StopAllCoroutines();

		rock.SendMessage("Reset");
		leftButton.SendMessage("Reset");
		rightButton.SendMessage("Reset");
		hand.SendMessage("Reset");
	}

	void GuideHandLeftButton(){
		leftButton.SendMessage("EnableButton", true);
		rightButton.SendMessage("EnableButton", true);
		hand.SendMessage("AnimateHand", leftButtonHandPos.position);
	}

	void GuideHandRightButton(){
		leftButton.SendMessage("EnableButton", true);
		rightButton.SendMessage("EnableButton", true);
		hand.SendMessage("AnimateHand", rightButtonHandPos.position);
	}

	void ButtonClicked(){
		hand.SendMessage("HideHand");
	}

	void NextMovement(){
		isMovingLeft = !isMovingLeft;

		if(isMovingLeft){
			GuideHandLeftButton();
		}

		else{
			GuideHandRightButton();
		}
	}
}
