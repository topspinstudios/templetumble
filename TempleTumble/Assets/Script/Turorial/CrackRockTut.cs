﻿using System.Collections;
using UnityEngine;

public class CrackRockTut : MonoBehaviour {
	int hp = 10;
	int currentHP;
	public SpriteRenderer crackRenderer;
	public SpriteRenderer myRenderer;
	public GameObject target;
	public GameObject smoke;
	Color crackColor = Color.white;
	bool isHypernating;
	bool isRaging;
	public GameObject rageButton;
	public GameObject controller;
	public ParticleSystem rockPS;
	public ParticleSystem smokePS;

	void Reset () {
		StopAllCoroutines();
		isRaging = false;
		isHypernating = false;
		myRenderer.enabled = true;
		crackColor.a = 0f;
		crackRenderer.color = crackColor;
		currentHP = hp;

		target.SetActive(isRaging);
	}

	void OnMouseDown(){
		CrackRock();
	}

	void CrackRock(){
		if(isHypernating == false){
			controller.SendMessage("StopHandAnim");

			if(isRaging){
				BreakRock();
				return;
			}
			else{
				rageButton.SendMessage("AddRage");
				currentHP--;

				if(currentHP <= 0){
					BreakRock();
					return;
				}

				crackColor.a = 1f - ((float)currentHP / (float)hp);
				crackRenderer.color = crackColor;
			}
			rockPS.Emit(5);
			smokePS.Emit(5);
		}
	}

	void BreakRock(){
		Instantiate(smoke, this.transform.position, this.transform.rotation);
		StartCoroutine("StartHypernation");
		crackColor.a = 0f;
		crackRenderer.color = crackColor;
		target.SetActive(false);
	}

	void StartRage(){
		target.SetActive(true);
		isRaging = true;
	}

	void EndRage(){
		target.SetActive(false);
		isRaging = false;
	}

	IEnumerator StartHypernation(){
		isHypernating = true;
		myRenderer.enabled = false;
		yield return new WaitForSeconds(1f);
		Reset();
	}
}
