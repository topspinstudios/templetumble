﻿using System.Collections;
using UnityEngine;

public class TouchColliderTut : MonoBehaviour {
	CircleCollider2D circleCollider;

	void Awake(){
		circleCollider = GetComponent<CircleCollider2D>();
	}

	void SetTouchPos(Vector2 pos){
		StopAllCoroutines();
		transform.position = pos;
		circleCollider.enabled = true;
		StartCoroutine("ActivateRoutine");
	}

	IEnumerator ActivateRoutine(){
		yield return null;
		circleCollider.enabled = false;
	}

	// Update is called once per frame
	void OnTriggerEnter2D (Collider2D col) {
		if(col.gameObject.tag == "Rock"){
			col.gameObject.SendMessage("CrackRock");
		}
	}
}
