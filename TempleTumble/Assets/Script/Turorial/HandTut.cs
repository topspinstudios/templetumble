﻿using System.Collections;
using UnityEngine;

public class HandTut : MonoBehaviour {
	SpriteRenderer spriteRen;
	Color transparent = new Color(1f, 1f, 1f, 0f);
	const float fadeInTime = 1.0f;

	void Reset(){
		StopAllCoroutines();
		if(!spriteRen)
			spriteRen = GetComponent<SpriteRenderer>();

		spriteRen.color = new Color(1f, 1f, 1f, 0f);
	}

	void HideHand(){
		spriteRen.color = transparent; // set it transparent
		StopAllCoroutines();
	}

	void AnimateHand(Vector3 handPos){
		StartCoroutine("FadeInHand", handPos);
	}

	IEnumerator FadeInHand(Vector3 handPos){
		float currentTime = 0f;
		Color newColor = new Color(1f, 1f, 1f, 0f);
		Vector3 startFadeInPos = new Vector3(handPos.x + 1.5f, handPos.y, handPos.z);

		while(true){
			Vector3 newPos = startFadeInPos;
			while(currentTime <= fadeInTime){
				newColor.a = Mathf.Lerp(0f, 1f, (currentTime / fadeInTime));
				spriteRen.color = newColor;

				newPos.x = Mathf.Lerp(newPos.x, handPos.x, (currentTime / fadeInTime));
				transform.position = newPos;

				currentTime += Time.fixedDeltaTime;
				yield return null;
			}

			yield return new WaitForSeconds(2.0f);
			currentTime = 0f;
		}
	}
}
