﻿using System.Collections;
using UnityEngine;

public class TouchObject : MonoBehaviour {
	public RagePower ragePower;
	CircleCollider2D circleCollider;
	AudioSource audioSource;
	bool isRaging;
	public DataController dataController;

	// Use this for initialization
	void Start() {
		circleCollider = GetComponent<CircleCollider2D>();
		audioSource = GetComponent<AudioSource>();
	}

	void SetTouchPos(Vector2 pos) {
		transform.position = new Vector3(pos.x, pos.y, 0f);
		StopAllCoroutines();
		circleCollider.enabled = false;

		StartCoroutine("TurnOnCollider");
	}

	IEnumerator TurnOnCollider(){
		circleCollider.enabled = true;
		yield return null;
		circleCollider.enabled = false;
	}

	void OnTriggerEnter2D(Collider2D col){
		if(col.gameObject.tag == "Rock"){
			if(dataController.isRaging == false){
				col.gameObject.SendMessage("CrackRock");
			}
			else{
				col.gameObject.SendMessage("CrushRock");
			}
			audioSource.Play();
			ragePower.AddRage();
		}
	}
}
