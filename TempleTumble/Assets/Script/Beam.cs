using System.Collections;
using UnityEngine;

public class Beam : MonoBehaviour {
	public AudioSource stageOneAS, stageTwoAS;

	public GameObject leftBeamAssitant;
	public GameObject rightBeamAssitant;
	Rigidbody2D body2D;
	public SpriteRenderer highlightRenderer;

	// ===== initial transform use for reset =====
	Vector3 startingPos;
	Quaternion startingRotation;

	public AnimationCurve rotatingAnimCurveNonePhysics;
	public AnimationCurve torqueCurve;
	const float totalBalanceTime = 3.0f; // total seconds for the beam to balance itself
	const float initialAngularDrag = 1.0f;
	const float minAngularDrag = 0.2f;
	float currentAngularDrag;
	bool isDecreasingAngularDrag;

	// Use this for initialization
	void Start () {
		body2D = GetComponent<Rigidbody2D>();
		startingPos = transform.position;
		startingRotation = transform.rotation;
		Physics2D.IgnoreLayerCollision(10, 11);
	}

	void StartMusic(){
		StartCoroutine("UpdateMusicLevel");
		StartCoroutine("EasyStart");
		isDecreasingAngularDrag = false;
		body2D.isKinematic = false;
		body2D.WakeUp();
	}

	void StopMusic(){
		StopCoroutine("UpdateMusicLevel");
		stageOneAS.volume = 0.0f;
		stageTwoAS.volume = 0.0f;

		transform.position = startingPos;
		transform.rotation = startingRotation;
		body2D.isKinematic = true;
		body2D.Sleep();

		leftBeamAssitant.SendMessage("StopAutoBalance");
		rightBeamAssitant.SendMessage("StopAutoBalance");
	}

	IEnumerator UpdateMusicLevel(){
		float zRotation;
		float stageOneMaxAngle = 0.2f;
		float stageTwoMaxAngle = 0.3f; // beam rotation at which peaks intense music volume

		while(true){
			zRotation = Mathf.Abs(transform.rotation.z);

			stageOneAS.volume = Mathf.Clamp(zRotation / stageOneMaxAngle, 0f, 1f);
			stageTwoAS.volume = Mathf.Clamp(zRotation / stageTwoMaxAngle, 0f, 1f);
			yield return new WaitForSeconds(0.2f);
		}
	}

	void Balance(){
		//StartCoroutine("RotateBeamNoPhysics");
		highlightRenderer.enabled = true;
		StartCoroutine("RotateBeamByTorque");
		leftBeamAssitant.SendMessage("StartAutoBalance");
		rightBeamAssitant.SendMessage("StartAutoBalance");
	}

	IEnumerator EasyStart(){
		body2D.angularDrag = 1.0f;
		yield return new WaitForSeconds(10f);
		StartCoroutine("DecreaseAngularDrag");
	}

	IEnumerator DecreaseAngularDrag(){
		isDecreasingAngularDrag = true;
		while(body2D.angularDrag > minAngularDrag){
			body2D.angularDrag -= 0.1f;
			yield return new WaitForSeconds(2f);
			isDecreasingAngularDrag = body2D.angularDrag > minAngularDrag;
		}
	}

	IEnumerator RotateBeamByTorque(){
		if(isDecreasingAngularDrag){
			StopCoroutine("DecreaseAngularDrag");
		}
		currentAngularDrag = body2D.angularDrag;

		float startZRotation = transform.rotation.z;

		if(Mathf.Abs(startZRotation) > 0.05f) {
			float currentTime = 0f;
			float torque = 0f;
			float maxTorque = 7000000f /* * (Mathf.Abs(startZRotation) / 0.15f)*/;
			maxTorque *= Mathf.Abs(transform.rotation.z) / 0.2f;
			float torqueCurveMultiplier = 0f;

			bool hasReachedCenterRotation = false;
			body2D.mass = 30000f;
			body2D.drag = 1000f;
			body2D.angularDrag = 100000f;
			float newMass = body2D.mass;
			body2D.gravityScale = 0f;

			while(!hasReachedCenterRotation && (currentTime < 2.0f)){
				torqueCurveMultiplier = torqueCurve.Evaluate(currentTime / 2.0f);
				torque = newMass * (torqueCurveMultiplier * maxTorque) * (startZRotation > 0 ? -1 : 1);
				body2D.AddTorque(torque);

				currentTime += Time.deltaTime;
				yield return null;

				hasReachedCenterRotation = Mathf.Abs(transform.rotation.z) <= 0.05f;
			}
			body2D.gravityScale = 1f;
		}

		StartCoroutine("EaseRotation");
		yield return new WaitForSeconds(2.1f); // Hold the rotation for 1 second before releasing it
		body2D.mass = 300f;
		body2D.drag = 0f;
		body2D.angularDrag = currentAngularDrag;
		body2D.constraints = RigidbodyConstraints2D.None;

		leftBeamAssitant.SendMessage("StopAutoBalance");
		rightBeamAssitant.SendMessage("StopAutoBalance");

		if(isDecreasingAngularDrag){
			StartCoroutine("DecreaseAngularDrag");
		}
	}

	IEnumerator EaseRotation(){
		float newRotation;
		float currentTime = 0.0f;
		float initialRotation = body2D.rotation;

		while(currentTime < 1.0f) {
			currentTime += Time.deltaTime;
			currentTime = Mathf.Clamp(currentTime, 0f, 1.0f);
			newRotation = Mathf.Lerp(initialRotation, 0.0f, currentTime);
			body2D.MoveRotation(newRotation);
			yield return null;
		}
		body2D.constraints = RigidbodyConstraints2D.FreezeRotation;

		StartCoroutine("MovePosition");
	}

	IEnumerator MovePosition(){
		float currentTime = 0f;
		Vector3 originPos = new Vector3(0f, -8.5f, 0f);
		Vector3 tiltedPos = transform.position;

		while(currentTime < 1.0f) {
			currentTime += Time.deltaTime;
			currentTime = Mathf.Clamp(currentTime, 0.0f, 1.0f);
			body2D.MovePosition(Vector3.Lerp(tiltedPos, originPos, currentTime));
			yield return null;
		}

		body2D.constraints = RigidbodyConstraints2D.None;
		highlightRenderer.enabled = false;
	}
}
