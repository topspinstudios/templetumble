﻿using UnityEngine;

public class BeamAssistant : MonoBehaviour {
	bool isAutoBalancing;

	void Start(){
		isAutoBalancing = false;
	}

	void StopAutoBalance(){
		isAutoBalancing = false;
		//print("disabling beam assistant");
	}

	void StartAutoBalance(){
		isAutoBalancing = true;
		//print("enabling beam assistant");
	}

	void OnTriggerEnter2D(Collider2D col){
		if(isAutoBalancing){
			//print(this.gameObject.name + " is colliding with: " + col.gameObject.tag);
			if(col.gameObject.tag == "Rock"){
				//print("Killing Rock");
				col.gameObject.SendMessage("SelfDestruct");
			}
		}
	}
}
