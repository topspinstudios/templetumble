﻿using System.Collections;
using UnityEngine;

public class DarkenPanel : MonoBehaviour {
	public Color mycolor;
	SpriteRenderer spriteRen;

	public AnimationCurve curve;

	// Use this for initialization
	void Start () {
		spriteRen = GetComponent<SpriteRenderer>();
		StartCoroutine("UpdateAlpha");
	}

	IEnumerator UpdateAlpha () {
		float currentTime = 0f;
		float randomTotalTime = GenerateRandomTimeScale();

		while(true){
			mycolor.a = curve.Evaluate(currentTime / randomTotalTime);
			spriteRen.color = mycolor;

			currentTime = currentTime + 0.05f;
			if(currentTime >= randomTotalTime){
				currentTime = 0f;
				randomTotalTime = GenerateRandomTimeScale();
			}

			yield return new WaitForSeconds(0.05f);
		}
	}

	float GenerateRandomTimeScale(){
		return Random.Range(5f, 15f);
	}
}
