﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHole : MonoBehaviour {
	PointEffector2D effector;
	public ParticleSystem particles;

	public Animator anim;

	float topY = 5f;
	float leftX = -13f;
	float bottomY = -5f;
	float rightX = 13f;

	// Update is called once per frame
	void Start () {
		effector = GetComponent<PointEffector2D>();
	}

	void StartGame(){
		StartCoroutine("BlackHoleRoutine");
	}

	IEnumerator BlackHoleRoutine(){
		yield return new WaitForSeconds(90f);

		while(true){
			yield return new WaitForSeconds(Random.Range(10f, 20f));

			StartCoroutine("EnableEffector");
			StartCoroutine("PlayParticles");
			transform.position = new Vector3(Random.Range(leftX, rightX), Random.Range(bottomY, topY), transform.position.z);
			anim.SetTrigger("AppearTrigger");
		}
	}

	IEnumerator EnableEffector(){
		yield return new WaitForSeconds(0.5f);
		effector.enabled = true;
		yield return new WaitForSeconds(1.5f);
		effector.enabled = false;
	}

	void StopGame(){
		anim.SetTrigger("GameOverTrigger");
		StopAllCoroutines();
		DisableBlackHole();
	}

	IEnumerator PlayParticles(){
		particles.Play();
		effector.enabled = true;

		yield return new WaitForSeconds(1.5f);
		DisableBlackHole();
	}

	void DisableBlackHole(){
		effector.enabled = false;
		particles.Stop();
	}
}
