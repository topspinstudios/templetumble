using UnityEngine;

public class RockHelper : MonoBehaviour {

	void OnCollisionStay2D(Collision2D col){
		if(col.gameObject.tag != "Column"){
			gameObject.SendMessage("DisableRockMovement");
			Destroy(this);
		}
	}
}
