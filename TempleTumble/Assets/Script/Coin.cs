using System.Collections;
using UnityEngine;

public class Coin : MonoBehaviour {

	AudioSource audioSource;
	CircleCollider2D circleCollider;

	GameObject gameController;

	void Start(){
		audioSource = GetComponent<AudioSource>();
		circleCollider = GetComponent<CircleCollider2D>();
	}

	void SetGameController(GameObject gameController){
		this.gameController = gameController;
	}

	void OnTriggerEnter2D(Collider2D col){
		if(col.gameObject.tag == "CoinCollider"){
			StartCoroutine("Shrink");
		}
	}

	// Shrink the coin after it collides
	IEnumerator Shrink(){
		PlayerPrefs.SetInt("CoinsCollected", (PlayerPrefs.GetInt("CoinsCollected") + 1));

		audioSource.Play();
		circleCollider.enabled = false;

		if(gameController != null)
			gameController.SendMessage("AddBonusScore");
		else
			StartCoroutine("WaitForGameController");

		while(this.transform.localScale.x > 0.1f){
			this.transform.localScale -= new Vector3(0.1f, 0.1f, 0.1f);

			yield return new WaitForSeconds(Time.deltaTime);
		}

		Destroy(this.gameObject);
	}

	IEnumerator WaitForGameController(){
		yield return new WaitForSeconds(0.2f);

	}
}
