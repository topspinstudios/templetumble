﻿using System.Collections;
using UnityEngine;

public class CameraShake : MonoBehaviour {
	const float minShakeIntensity = 0.02f;
	const float maxShakeIntensity = 0.06f;
	Vector3 startPos;

	// Update is called once per frame
	void Start () {
		startPos = transform.position;
		StartCoroutine("Shake");
	}

	IEnumerator Shake(){
		float shakeTime = 0.5f;
		float currentTime = 0f;
		Vector3 newPos;
		float shakeIntensity;

		while(true){
			currentTime = 0f;
			yield return new WaitForSeconds(Random.Range(3f, 7f));
			shakeIntensity = Random.Range(minShakeIntensity, maxShakeIntensity);

			while(currentTime < shakeTime){
				newPos = new Vector3(Random.Range(-1f, 1f) * shakeIntensity, Random.Range(-1f, 1f) * shakeIntensity, startPos.z);
				transform.transform.position = newPos;
				currentTime += 0.04f;
				yield return new WaitForSeconds(0.04f);
			}

			transform.position = startPos;
		}
	}
}
