﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {
	public GameObject TutorialController;

	GameObject gameController;
	public GameObject introHelpText, mainGamePanel, gameoverPanel, homePanel, achievementsPanel, tutorialPanel;
	public Text mainGameScoreText, gameOverScoreText;
	private int score;
	private float scoreRollUpTotalTime = 2.0f, scoreRollUpFPS = 10f; // The time it takes to roll up the score to final score
	public AnimationCurve scoreAnimCurve; // helps lerp scores in game over screen

	private int noPowerUpScore;
	private bool hasUsedPowerUp;

	void Start () {
		gameController = transform.root.gameObject;
		CheckIfFirstTime();
	}

	void CheckIfFirstTime(){
		int hasPlayed = PlayerPrefs.GetInt("hasPlayed");
		if(hasPlayed != 1){
			TutorialButton();
		}
		PlayerPrefs.SetInt("hasPlayed", 1);
	}

	public void PlayButton(){
		gameController.SendMessage("PlayGame");
		StartCoroutine("DisplayIntroHelpTextRoutine");
		StartCoroutine("StartScore");
		homePanel.SetActive(false);
		gameoverPanel.SetActive(false);
		mainGamePanel.SetActive(true);

		hasUsedPowerUp = false;
	}

	public void PlayAgainButton(){
		PlayButton();
	}

	public void AchievementsButton(){
		homePanel.SetActive(false);
		achievementsPanel.SetActive(true);

		achievementsPanel.SendMessage("UpdateAchievements");
	}

	public void TutorialButton(){
		homePanel.SetActive(false);
		tutorialPanel.SetActive(true);
		TutorialController.SendMessage("StartTutorial");
	}

	public void HomeButton(){
		gameoverPanel.SetActive(false);
		mainGamePanel.SetActive(false);
		achievementsPanel.SetActive(false);
		tutorialPanel.SetActive(false);
		homePanel.SetActive(true);
	}

	IEnumerator DisplayIntroHelpTextRoutine(){
		introHelpText.SetActive(true);
		yield return new WaitForSeconds(3f); // Display text for 3 seconds
		introHelpText.SetActive(false);
	}

	IEnumerator StartScore(){
		this.score = 0;
		mainGameScoreText.text = "Score: " + score;

		while(true){
			yield return new WaitForSeconds(1f);
			this.score += 100;
			mainGameScoreText.text = "Score: " + this.score;
		}
	}

	void AddBonusScore(){
		this.score += 100;
		mainGameScoreText.text = "Score: " + this.score;
		AttractScore();
	}

	void AttractScore(){
		StopCoroutine("GrowScoreText");
		StartCoroutine("GrowScoreText");
	}

	IEnumerator GrowScoreText(){
		yield return new WaitForSeconds(0.25f);

		float currentDeltaTime = 0.0f;
		Vector3 biggestSize = new Vector3(2f, 2f, 2f);
		mainGameScoreText.transform.localScale = biggestSize;

		yield return new WaitForSeconds(0.25f);

		float shrinkTime = 0.25f;
		while(currentDeltaTime < shrinkTime){
			mainGameScoreText.transform.localScale = Vector3.Lerp(biggestSize, Vector3.one, (currentDeltaTime / shrinkTime));
			//mainGameScoreText.transform.Translate(Vector3.up * 0.5f);
			currentDeltaTime += Time.deltaTime;
			yield return null;
		}
	}

	void GameOver(){
		StopCoroutine("StartScore");
		StartCoroutine("RollUpGameOverScoreText");
		gameoverPanel.SetActive(true);
		homePanel.SetActive(false);
		mainGamePanel.SetActive(false);

		PlayerPrefs.SetInt("HighScore", this.score);
		if(!hasUsedPowerUp){
			int noPowerUpScore = PlayerPrefs.GetInt("NoPowerUpScore");
			PlayerPrefs.SetInt("NoPowerUpScore", this.score < noPowerUpScore ? this.score : noPowerUpScore);
		}
	}

	IEnumerator RollUpGameOverScoreText(){
		int currentScore = 0;
		float currentTime = 0f;
		gameOverScoreText.text = "Score: " + currentScore;

		while(currentTime <= scoreRollUpTotalTime){
			yield return new WaitForSeconds(1f / scoreRollUpFPS);
			currentTime += 1f / scoreRollUpFPS; // Update current time

			currentScore = (int)Mathf.Lerp(0, this.score, scoreAnimCurve.Evaluate(currentTime / scoreRollUpTotalTime));
			gameOverScoreText.text = "Score: " + currentScore;
		}
	}

	// This function is used to determine if the player has used any power up to keep track of score
	void UsePowerUp(){
		hasUsedPowerUp = true;
	}
}
