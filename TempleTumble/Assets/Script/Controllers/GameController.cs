using UnityEngine;
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Advertisements;

public class GameController : MonoBehaviour{
  public GameObject uiController;
  public GameObject mainGame;
  public GameObject coin;
  public GameObject curseEyes;
  public GameObject leftSpikeBase;
  public GameObject rightSpikeBase;
  public GameObject blackHole;
  public DataController dataController;
  public RagePower ragePower;
  List<GameObject> activeCoins;

  // GameInfo
  public Transform leftBound;
  public Transform rightBound;
  public Transform initialRockStartPos;

  public AnimationCurve[] difficultyAnimationCurves; // Time before the next spawn
  public AnimationCurve rockMovementCurve; // Use this to calculate rock movement

  public AudioClip[] rockHitsRockSFXArray;
  public AudioClip rockHitsBeamSFX;

  public List<GameObject> activeRockList;
  public List<bool> isActiveRockMoveableList;
  Dictionary<int, GameObject> rockIdentifierDict; // Use dictionary to determine which rock to remove from active list
  GameObject[] rockPrefabArray;
  GameObject rockCrusherPrefab;
  GameObject currentrockCrusher;
  GameInfo gameInfo;
  bool canSummonNewRock;

  int loseCount = 0;
  int totalPlayTime = 0;

  void Start(){
    Screen.autorotateToLandscapeRight = true;
    Screen.autorotateToLandscapeLeft = true;
    Screen.autorotateToPortrait = false;
    Screen.autorotateToPortraitUpsideDown = false;

    Screen.orientation = ScreenOrientation.AutoRotation;

    LoadPrefabs();
    LoadSFX();
    SetGameInfo();

    Advertisement.Initialize(dataController.advertisementID);
  }

  // Load sound effects for rock collision
  void LoadSFX(){
    rockHitsRockSFXArray = Resources.LoadAll<AudioClip>(@"SFX\RockHitsRock");
    rockHitsBeamSFX = Resources.Load<AudioClip>(@"SFX\RockHitsBeam\Rock_Hit_Beam");
  }

  // Load Prefabs -- Rocks and Rock Killers
  void LoadPrefabs(){
    rockPrefabArray = Resources.LoadAll<GameObject>("RockPrefabs/");
    rockCrusherPrefab = Resources.Load<GameObject>("RockCrusher");
  }

  void SetGameInfo(){
    this.gameInfo = new GameInfo();
    this.gameInfo.leftPlayableBound = leftBound.position.x;
    this.gameInfo.rightPlayableBound = rightBound.position.x;
    this.gameInfo.initialPositionY = initialRockStartPos.position.y;
  }

  void PlayGame(){
    //mainGame.SendMessage("PlayGame"); // Set all object active
    //StartCoroutine("SummonRocksRoutine");
    StartCoroutine("SummonRockCrusherRoutine");
    //this.gameObject.SendMessage("CheckTouchInputRoutine");
    mainGame.SendMessage("StartMusic");
    SendMessage("StartGame");
    activeRockList = new List<GameObject>();
    canSummonNewRock = true;

    StartCoroutine("StartDelay");
    StartCoroutine("SummonCoin");
    StartCoroutine("CurseRock");
    leftSpikeBase.SendMessage("StartSlide");
    rightSpikeBase.SendMessage("StartSlide");
    blackHole.SendMessage("StartGame");
    //StartCoroutine("SummonRocksRoutine");

    ragePower.ResetRage();
    StartCoroutine("StartPlayTime"); // use this time to play Advertisement
    dataController.isRaging = false;
  }

  IEnumerator CurseRock(){
    GameObject cursedRock;

    yield return new WaitForSeconds(10.0f);

    while(true){
      yield return new WaitForSeconds(Random.Range(5, 9));
      cursedRock = activeRockList[activeRockList.Count - 1];
      if(cursedRock){
        cursedRock.SendMessage("TriggerCurse");
        curseEyes.SendMessage("TriggerCurse");
      }
    }
  }

  IEnumerator StartDelay(){
    yield return new WaitForSeconds(1f);
    SummonRock();
    yield return new WaitForSeconds(5f);
    StartCoroutine("ChangeRockSizeRoutine");
  }

  void SummonRock(){
    if(canSummonNewRock){
      StopCoroutine("SummonRockFailSafe");
      StartCoroutine("SummonRockFailSafe");

      int randomRockIndex = Random.Range(0, rockPrefabArray.Length);
      Vector3 randomPosition = new Vector3(Random.Range(gameInfo.leftPlayableBound, gameInfo.rightPlayableBound), gameInfo.initialPositionY, 0f);
      GameObject rock = Instantiate(rockPrefabArray[randomRockIndex], randomPosition, Quaternion.identity);

      SetRockProperties(rock);
      activeRockList.Add(rock);
      isActiveRockMoveableList.Add(true);

      if(Random.Range(0, 100) < 30){
        rock.SendMessage("AddSpeed", Random.Range(0f, Mathf.Clamp(100f, 800f, (float)totalPlayTime)));
      }

      StartCoroutine("SummonRockCoolDown");
    }
  }

  IEnumerator ChangeRockSizeRoutine(){
    while(true){
      yield return new WaitForSeconds(Random.Range(7f, 15f));

      GameObject rockToChange;
      do{
        rockToChange = activeRockList[Random.Range(0, activeRockList.Count - 1)];

        if(rockToChange != null ){
          rockToChange.SendMessage("ChangeToRandomSize");
        }
      } while(rockToChange == null);
    }
  }

  IEnumerator SummonCoin(){
    activeCoins = new List<GameObject>();

    while(true){
      yield return new WaitForSeconds(Random.Range(4f, 8f));

      Vector3 randomPosition = new Vector3(Random.Range(gameInfo.leftPlayableBound, gameInfo.rightPlayableBound),
                                            Random.Range(0f, 8f), 0f);
      GameObject currentCoin = Instantiate(coin, randomPosition, Quaternion.identity);
      currentCoin.SendMessage("SetGameController", this.uiController);
      activeCoins.Add(currentCoin);
    }
  }

  // Will spawn new rock if game controller doesn't receive a message to spawn a new rock
  IEnumerator SummonRockFailSafe(){
    yield return new WaitForSeconds(3.0f); // wait three seconds before summoning another rock
    SummonRock();
  }

  IEnumerator SummonRockCoolDown(){
    canSummonNewRock = false;
    yield return new WaitForSeconds(1f);
    canSummonNewRock = true;
  }

  IEnumerator SummonRocksRoutine(){
    int randomRockIndex;
    GameObject rock;
    Vector3 randomPosition;

    while(true){
      randomRockIndex = Random.Range(0, rockPrefabArray.Length);
      randomPosition = new Vector3(Random.Range(gameInfo.leftPlayableBound, gameInfo.rightPlayableBound), gameInfo.initialPositionY, 0f);
      rock = Instantiate(rockPrefabArray[randomRockIndex], randomPosition, Quaternion.identity);

      if(dataController.isRaging)
        rock.SendMessage("EnableRage", true);

      SetRockProperties(rock);
      activeRockList.Add(rock);
      isActiveRockMoveableList.Add(true);

      // Get a random wait time from the animation curve
      yield return new WaitForSeconds(1.0f);
    }
  }

  // provide rocks with properties
  void SetRockProperties(GameObject rock){
    int randomSFX = Random.Range(0, rockHitsRockSFXArray.Length - 1);
    AudioClip[] SFX = new AudioClip[]{rockHitsRockSFXArray[randomSFX], rockHitsBeamSFX};
    rock.SendMessage("StartMovement");
    rock.SendMessage("EnableRage", dataController.isRaging);
  }

  // provide rock crusher with properties
  void SetRockCrusherProperties(GameObject rockCrusher){
    int randomSFX = Random.Range(0, rockHitsRockSFXArray.Length - 1);
    AudioClip[] SFX = new AudioClip[]{rockHitsRockSFXArray[randomSFX], rockHitsBeamSFX};
    rockCrusher.SendMessage("SetSFX", SFX);
  }

  void RemoveDestroyedRock(){
    for(int i = activeRockList.Count - 1; i >= 0; i--)
      if(activeRockList[i] == null){
        activeRockList.RemoveAt(i); // Remove rock that's null
        isActiveRockMoveableList.RemoveAt(i); // Remove rock that's null
      }
  }

  IEnumerator SummonRockCrusherRoutine(){
    int minimumActiveRockCount = 5;
    yield return new WaitForSeconds(10.0f);

    while(true){
      int activeAliveRockCount = 0;
      foreach(GameObject rock in activeRockList)
        activeAliveRockCount += rock != null ? 1 : 0;

      if(activeAliveRockCount > minimumActiveRockCount){
        Vector3 randomPosition = new Vector3(Random.Range(gameInfo.leftPlayableBound, gameInfo.rightPlayableBound), gameInfo.initialPositionY, 0f);
        currentrockCrusher = Instantiate(rockCrusherPrefab, randomPosition, Quaternion.identity);
        SetRockCrusherProperties(currentrockCrusher); // Set the properties for rock crusher
      }
      yield return new WaitForSeconds(Random.Range(5f, 8f));
    }
  }

  void PauseGame(){
    Time.timeScale = 0f;
    StopCoroutine("StartPlayTime");
  }

  void ResumeGame(){
    Time.timeScale = 1f;
  }

  void GameOver(){
    // Reset main game objects to original position and rotation
    StopAllCoroutines();
    foreach(GameObject rock in activeRockList){
      if(rock != null)
        Destroy(rock);
    }

    foreach(GameObject coin in activeCoins){
      if(coin != null)
        Destroy(coin);
    }

    if(currentrockCrusher != null)
      Destroy(currentrockCrusher);

    mainGame.SendMessage("StopMusic");
    uiController.SendMessage("GameOver");
    leftSpikeBase.SendMessage("StopGame");
    rightSpikeBase.SendMessage("StopGame");
    blackHole.SendMessage("StopGame");

    ragePower.ResetRage();
    TargetAllActiveRocks();

    ShowAdvertisement();
  }

  void ShowAdvertisement(){
    if(Advertisement.IsReady() == false){
      return;
    }

    if(totalPlayTime < 45){
      loseCount++;
      if(loseCount > 1){
        Advertisement.Show();
        loseCount = 0;
      }
    }
    else{
      Advertisement.Show();
    }
  }

  IEnumerator StartPlayTime(){
    totalPlayTime = 0;

    while(true){
      yield return new WaitForSeconds(1f);
      totalPlayTime++;
    }
  }

  // Active or deactivate targets on rocks for rage mode
  public void TargetAllActiveRocks(){
    foreach(GameObject rock in activeRockList){
      if(rock != null){
        rock.SendMessage("EnableRage", dataController.isRaging);
      }
    }
  }
}
