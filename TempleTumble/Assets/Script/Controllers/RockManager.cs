using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This class is for caching rock rather than create and destroy them.
public class RockManager : MonoBehaviour {
	public GameObject gameController;
	public List<AudioClip> rockSFX;
	public AudioClip rockHitsBeamSFX;
	public GameInfo gameInfo;

	List<GameObject> rockList;
	List<GameObject> activeRockList;
	float totalPlayTime;
	bool canSummonNewRock;
	// Use this for initialization
	void Start () {
		SetRockGroup();
	}

	void SetRockGroup(){
		int rockCount = transform.childCount;
		rockList = new List<GameObject>();

		for (int rockIndex = 0; rockIndex < rockCount; rockIndex++) {
			GameObject currentRock = transform.GetChild(rockIndex).gameObject;
			currentRock.GetComponent<Rock>().rockHitsRock = rockSFX[Random.Range(0, rockSFX.Count)];
			currentRock.GetComponent<Rock>().rockHitsBeam = rockHitsBeamSFX;
			currentRock.GetComponent<Rock>().gameController = gameController;

			rockList.Add(currentRock);
		}
	}

	void StartGame(){
		StopAllCoroutines();
		canSummonNewRock = true;
		StartCoroutine("StartDelay");
		StartCoroutine("StartTimeCounter");
	}

	void EndGame(){
		StopAllCoroutines();
		DisableAllRock();
	}

	IEnumerator StartDelay(){
    yield return new WaitForSeconds(1f);
    SummonRock();
    yield return new WaitForSeconds(5f);
    StartCoroutine("ChangeRockSizeRoutine");
  }

  void SummonRock(){
		if(!canSummonNewRock)
			return;

    StopCoroutine("SummonRockFailSafe");
    StartCoroutine("SummonRockFailSafe");

    int randomRockIndex = Random.Range(0, rockList.Count - 1);
    Vector3 randomPosition = new Vector3(Random.Range(gameInfo.leftPlayableBound, gameInfo.rightPlayableBound), gameInfo.initialPositionY, 0f);
    GameObject rock = rockList[randomRockIndex];
		rock.transform.position = randomPosition;

    activeRockList.Add(rock);

    if(Random.Range(0, 100) < 30){
      rock.SendMessage("AddSpeed", Random.Range(0f, Mathf.Clamp(100f, 800f, totalPlayTime)));
    }
		rock.SendMessage("StartMovement");

    StartCoroutine("SummonRockCoolDown");
  }

	// Will spawn new rock if game controller doesn't receive a message to spawn a new rock
	IEnumerator SummonRockFailSafe(){
		yield return new WaitForSeconds(3.0f); // wait three seconds before summoning another rock
		SummonRock();
	}

	IEnumerator SummonRockCoolDown(){
		canSummonNewRock = false;
		yield return new WaitForSeconds(1f);
		canSummonNewRock = true;
	}

	IEnumerator ChangeRockSizeRoutine(){
    while(true){
      yield return new WaitForSeconds(Random.Range(7f, 15f));

      GameObject rockToChange;
      do{
        rockToChange = activeRockList[Random.Range(0, activeRockList.Count - 1)];

        if(rockToChange != null ){
          rockToChange.SendMessage("ChangeToRandomSize");
        }
      } while(rockToChange == null);
    }
  }

	void DisableAllRock(){
		for(int i = activeRockList.Count - 1; i >= 0; i--){
			GameObject rock = activeRockList[i];
			rock.transform.position = this.transform.position;

			rock.SetActive(false);
			rockList.Add(rock);
			activeRockList.RemoveAt(i);
		}
	}

	IEnumerator StartTimeCounter(){
		totalPlayTime = 0.0f;
		while(true){
			yield return new WaitForSeconds(1.0f);
			totalPlayTime += 1.0f;
		}
	}
}
