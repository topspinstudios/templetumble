using UnityEngine;

public class DataController : MonoBehaviour {
	public string advertisementID {
		get {return "1774849";}
		private set {}
	}

	[HideInInspector]
	public bool isRaging;

	public int rageMaxTapCount {
		get {return 20;}
		private set {}
	}

	public float maxRageTime{
		get {return 3.0f;}
		private set {}
	}

	public float UpdateTime{
		get {return Time.fixedDeltaTime;}
		private set {}
	}
}
