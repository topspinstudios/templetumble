using UnityEngine;

public class MainGameObjectController : MonoBehaviour {

	public Transform beam;

	void StartMusic(){
		beam.SendMessage("StartMusic");
	}

	void StopMusic(){
		beam.SendMessage("StopMusic");
	}
}
