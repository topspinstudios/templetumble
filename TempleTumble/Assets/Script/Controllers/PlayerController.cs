﻿using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	private bool isClicked;
	public GameObject touchObject;
	public RagePower ragePower;

	void Start(){
		isClicked = false;
		StartCoroutine("CheckTouchInputRoutine");
	}

	// Check for player's touch input on rock
	IEnumerator CheckTouchInputRoutine(){
		while(true){
			if(!isClicked){
				if(Input.GetMouseButtonDown(0) || Input.GetMouseButton(0)){
					isClicked = true;
					Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

					touchObject.SendMessage("SetTouchPos", touchPos);
				}
			}
			if(Input.GetMouseButtonUp(0))
				isClicked = false;

			yield return null;
		}
	}
}
